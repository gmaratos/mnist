# silence absl

from absl import logging
logging.set_verbosity(logging.ERROR)

# import preamble

import json
import os.path as osp
from types import SimpleNamespace

import numpy as np
import jax as jx
import jax.random as jrd
import jax.numpy as jnp
import flax.linen as fln
import flax.serialization as fls
import flax.core.frozen_dict as fld
import optax as opt
import sklearn.metrics as skm
from tqdm import tqdm

# dataset object

def onehot(labels, num_labels=10):

    size = len(labels)
    targets = np.zeros((size, num_labels))
    targets[np.arange(size), labels] = 1
    return targets

class MNIST:

    def __init__(self, root_path, typ, iters, bs=1):

        names = (f'{typ}_{suffix}.npy' for suffix in ['x', 'y'])
        paths = (osp.join(root_path, name) for name in names)
        self.data, self.targets = [np.load(path) for path in paths]
        self.batch_size = bs
        self.length = 0
        self.iters = iters

    def __iter__(self): ## stream style iterator

        ## set up batch inds
        size = len(self.data)
        inds = np.arange(size)
        bs = self.batch_size
        batches = [inds[i:i+bs] for i in range(0, size, bs)]
        iters = self.iters if self.iters > 0 else len(batches)
        ## build stream iterator
        inds = (batches[i % len(batches)] for i in range(iters))
        gen = ((self.data[ind], onehot(self.targets[ind])) for ind in inds)
        final = enumerate(gen, start=1)
        return iter(tqdm(final, total=iters))

# model

class CNN(fln.Module):

    @fln.compact
    def __call__(self, x):

        x = x.reshape(*x.shape, 1) # mnist is missing the channels
        x = fln.Conv(features=32, kernel_size=(3, 3))(x)
        x = fln.relu(x)
        x = fln.avg_pool(x, window_shape=(2, 2), strides=(2, 2))
        x = x.reshape((x.shape[0], -1))
        x = fln.Dense(features=128)(x)
        x = fln.Dense(features=10)(x)
        return x

model = CNN()

# optimizer

@jx.jit
def loss(params, x, y):

    logits = model.apply(params, x)
    losses = opt.softmax_cross_entropy(logits, y)
    return jnp.mean(losses)

def validate(params, dataloader):

    result = []
    predictions, true = [], []
    for i, (x, y) in dataloader:
        logits = model.apply(params, x)
        predictions.append(logits)
        true.append(y)
    true = jnp.concatenate(true)
    predictions = jnp.concatenate(predictions)
    return predictions, true

def fit(params, train_dataloader, dev_dataloader=None, eval_after=10):

    def process(train_state):
        preds, true = train_state.preds, train_state.true
        preds = onehot(preds.argmax(1))
        macro = skm.f1_score(preds, true, average='macro')
        tloss = np.mean(train_state.lvals)
        dloss = jnp.mean(opt.softmax_cross_entropy(preds, true)).item()
        metrics = dict(
            MF1 = round(macro, 3),
            tloss = round(tloss, 3),
            dloss = round(dloss, 3),
        )
        log_message = json.dumps(metrics) + '\n'
        with open('train.log', 'a') as f:
            f.write(log_message)
        train_state.lvals = []; train_state.preds = None; train_state.true = None
        if macro > train_state.best_score: ## early stopping
            name = 'params.msgpack'
            train_state.best_score = macro
            thawed_params = fld.unfreeze(params)
            with open(name, 'wb') as f:
                serialized = fls.msgpack_serialize(thawed_params)
                f.write(serialized)
        return train_state

    optimizer = opt.sgd(learning_rate=1e-3)
    opt_state = optimizer.init(params)

    def update(params, opt_state, x, y):

        lval, gradient = jx.value_and_grad(loss)(params, x, y)
        updates, opt_state = optimizer.update(gradient, opt_state, params)
        return lval, jx.tree_multimap(lambda p, u: p + u, params, updates), opt_state

    train_state = SimpleNamespace(best_score=0, lvals=[], preds=None, true=None)
    for i, (x, y) in train_dataloader:
        lval, params, opt_state = update(params, opt_state, x, y)
        train_state.lvals.append(lval.item())
        if (i % eval_after == 0) or (i == 1):
            preds, true = validate(params, dev_dataloader)
            train_state.preds, train_state.true = preds, true
            train_state = process(train_state)
    return params

# main script

batch_size = 100
key = jrd.PRNGKey(0)
path = '/srv/datasets/mnist'
train_dataloader = MNIST(path, 'train', 300, bs=batch_size)
dev_dataloader = MNIST(path, 'dev', -1, bs=batch_size)
batch = train_dataloader.data[:5]
params = model.init(key, batch)
params = fit(params, train_dataloader, dev_dataloader, eval_after=100)
