# import preamble

from numpy import load, save
from os.path import join
from random import shuffle

# load the training data

root_path = '/srv/datasets/mnist'
names = (f'train_{suffix}.npy' for suffix in ['x', 'y'])
paths = (join(root_path, 'original', name) for name in names)
data, targets = [load(path) for path in paths]

# reserve 20% for validation and save splits

size = len(data)
dev_size = int(size*0.2)
inds = list(range(size))
shuffle(inds)
dev_inds, train_inds = inds[:dev_size], inds[dev_size:]
for name, inds in zip(['train', 'dev'], [train_inds, dev_inds]):
    data_x, data_y = data[inds], targets[inds]
    for typ, d in zip(['x', 'y'], [data_x, data_y]):
        path = join(root_path, f'{name}_{typ}.npy')
        save(path, d)
